/*Теоретичні питання
1. Яке призначення методу event.preventDefault() у JavaScript?
Запобігти дефолтній поведінці браузера і натомість прописати свою логіку. Наприклад натискання на посилання яке за замовчуванням відправляє нас по URL адресі.
Цю дію ми можемо відмінити з допомогою preventDefault().

2. В чому сенс прийому делегування подій?
Прийом делегування подій в JavaScript дозволяє ефективно керувати обробкою подій для декількох елементів,
зокрема для великої кількості або динамічно створюваних елементів. Замість того, щоб додавати обробники подій 
безпосередньо до кожного окремого елемента, ви можете додати один обробник до їх спільного батьківського елемента.

3. Які ви знаєте основні події документу та вікна браузера?
DOMContentLoaded – браузер повністю завантажив HTML, і дерево DOM побудовано, але зовнішні ресурси, такі як зображення <img> та стилі, можуть ще не бути завантажені.
load – завантажено не тільки HTML, але і всі зовнішні ресурси: зображення, стилі тощо.
beforeunload/unload – користувач залишає сторінку.

Практичне завдання:
Реалізувати перемикання вкладок (таби) на чистому Javascript.*/

const tabs = document.querySelectorAll('.tabs-title');
const tabContents = document.querySelectorAll('.tabs-content li');

document.querySelector(".tabs").addEventListener('click', function(event) {
    tabs.forEach(tab => {
        if (tab === event.target) {
            tab.classList.add('active');
        } else {
            tab.classList.remove('active');
        }
    });
    tabContents.forEach(tabContent => {
        if (tabContent.id === event.target.textContent) {
            tabContent.classList.replace('tabs-content__item', 'tabs-content__item-active');
        } else {
            tabContent.classList.replace('tabs-content__item-active', 'tabs-content__item');
        }
    })
});

